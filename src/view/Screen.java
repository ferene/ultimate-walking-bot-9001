package view;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.concurrent.BlockingQueue;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import controller.dto.DestinationDto;
import controller.dto.RobotDto;
import controller.event.CleanEvent;
import controller.event.ControllerEvent;
import controller.event.KeyPressedEvent;
import controller.event.LearnStatusEvent;
import controller.event.MoveRobotEvent;
import controller.event.SetDestinationPositionEvent;
import controller.event.SetRobotPositionEvent;

public class Screen extends JFrame
{
	private static final long serialVersionUID = 1L;
	private final static int DISTANCE_TO_CENTER_FROM_LEFT = 8;
	private final int BACKGROUND_WIDTH = 400;
	private final int BACKGROUND_HEIGHT = 400;
	/* kolejka blokujaca */
	private final BlockingQueue<ControllerEvent> queue;
	private final JPanel panel;
	private final JPanel northPanel;
	private final JPanel centerPanel;
	private final JPanel eastPanel;
	private ImageIcon backgroundImage;
	private ImageIcon robotImage;
	private ImageIcon destinationImage;

	private final JLabel background;
	private final JLabel robotButton;
	private final Robot robott;
	private final JLabel destinationButton;
	private final JLabel destination;
	
	private final Cursor robotCursor;
	private final Cursor arrowCursor;
	private final Cursor destinationCursor;
	/* okienko pliku */
	private JButton fileButton;
	private final PopUpWindow popUpWindow;
	
	private RobotDto robotDto;
	private DestinationDto destinationDto;
		
	private JButton moveButton;
	
	private JRadioButton learn;
	
	private JButton cleanButton;

	
	/**
	 * konstruktor
	 * 
	 * @param queue - kolejka blokujaca
	 */
	public Screen(final BlockingQueue<ControllerEvent> queue)
	{
		super();
		this.queue = queue;
		
		panel = new JPanel();
		panel.setLayout(new BorderLayout());
		northPanel = new JPanel();
		northPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		centerPanel = new JPanel();
		centerPanel.setLayout(null);
		eastPanel = new JPanel();
		eastPanel.setLayout(new GridLayout(10,1));

		setTitle("Ultimate Walking Bot 9001");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setSize(588,504);
		setResizable(false);
        getContentPane().add(panel);
        
        robotImage = new ImageIcon("img/robot8.png");
        destinationImage = new ImageIcon("img/destination.png");
        destinationButton = new JLabel("ustaw polozenie mety");
        destinationButton.setIcon(destinationImage);
        background = new JLabel();
        robotButton = new JLabel("ustaw polozenie robota");
        robotButton.setIcon(robotImage);
        robott = new Robot();
        destination = new JLabel(destinationImage);
        robotCursor = getCursorFromImage(robotImage);
        arrowCursor = Cursor.getDefaultCursor();
        destinationCursor = getCursorFromImage(destinationImage);
        
        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(new MyDispatcher());
        
        addComponents();
        popUpWindow = new PopUpWindow(queue);
		setVisible(true);
	}
	
	private class MyDispatcher implements KeyEventDispatcher
    {

		@Override
        public boolean dispatchKeyEvent(KeyEvent e) 
		{
            if (e.getID() == KeyEvent.KEY_PRESSED) 
            {
            	queue.add(new KeyPressedEvent(e.getKeyChar()));
            } 
            else if (e.getID() == KeyEvent.KEY_RELEASED) 
            {} 
            else if (e.getID() == KeyEvent.KEY_TYPED) 
            {}
            return false;
        }
    }
	
	public void addComponents()
	{
		panel.add(northPanel, BorderLayout.NORTH);
		panel.add(centerPanel, BorderLayout.CENTER);
		panel.add(eastPanel, BorderLayout.EAST);
		
		
		
		fileButton = new JButton("open file");
		fileButton.addMouseListener(new MouseListener() 
		{
			@Override
			public void mouseReleased(MouseEvent arg0){}
			
			@Override
			public void mousePressed(MouseEvent arg0){}
			
			@Override
			public void mouseExited(MouseEvent arg0){}
			
			@Override
			public void mouseEntered(MouseEvent arg0){}
			
			@Override
			public void mouseClicked(MouseEvent arg0)
			{
				popUpWindow.setVisible();
			}
		});;
		
		northPanel.add(fileButton);
		
		backgroundImage = new ImageIcon();
		background.setIcon(backgroundImage);
		background.addMouseListener(new MouseListener() 
		{	
			@Override
			public void mouseReleased(MouseEvent arg0){}
			
			@Override
			public void mousePressed(final MouseEvent event) 
			{
				if(getCursor() == robotCursor)
				{
					queue.add(new SetRobotPositionEvent(
							((float)(event.getX() + DISTANCE_TO_CENTER_FROM_LEFT))/BACKGROUND_WIDTH,
							((float)(event.getY() - robott.getIcon().getIconHeight()/2))/BACKGROUND_HEIGHT));
				}
				if(getCursor() == destinationCursor)
				{
					queue.add(new SetDestinationPositionEvent(
							(float)((event.getX() + (float)destination.getPreferredSize().getWidth()/2))/BACKGROUND_WIDTH, 
							((float)(event.getY() - (float)destination.getPreferredSize().getHeight()/2))/BACKGROUND_HEIGHT));
				}
				setCursor(arrowCursor);
			}
			
			@Override
			public void mouseExited(MouseEvent arg0){}
			
			@Override
			public void mouseEntered(MouseEvent arg0){}
			
			@Override
			public void mouseClicked(MouseEvent arg0){}
		});
		
		centerPanel.add(robott);
		centerPanel.add(destination);
		centerPanel.add(background);

		
		robott.setVisible(false);
		robott.setBounds((int) (0 - robott.getPreferredSize().getWidth()/2.5), 0 - (int)robott.getPreferredSize().getHeight()/2, (int)robott.getPreferredSize().getWidth(), (int)robott.getPreferredSize().getHeight());
		
		destination.setVisible(false);
		destination.setBounds(0, 0, destination.getIcon().getIconWidth(), destination.getIcon().getIconHeight());
		
		eastPanel.add(robotButton);
		eastPanel.add(destinationButton);
		destinationButton.addMouseListener(new MouseListener() 
		{	
			@Override
			public void mouseReleased(MouseEvent e){}
			
			@Override
			public void mousePressed(MouseEvent e){}
			
			@Override
			public void mouseExited(MouseEvent e){}
			
			@Override
			public void mouseEntered(MouseEvent e){}
			
			@Override
			public void mouseClicked(final MouseEvent event)
			{
				if(background.getIcon().getIconHeight() >= 0)
				{
					setCursor(destinationCursor);
				}
			}
		});
		
		robotButton.addMouseListener(new MouseListener() 
		{	
			@Override
			public void mouseReleased(MouseEvent arg0){}
			
			@Override
			public void mousePressed(MouseEvent arg0) 
			{
				if(background.getIcon().getIconHeight() >= 0)
				{
					setCursor(robotCursor);
				}
			}
			
			@Override
			public void mouseExited(MouseEvent arg0){}
			
			@Override
			public void mouseEntered(MouseEvent arg0){}
			
			@Override
			public void mouseClicked(MouseEvent arg0){}
		});
		
		moveButton = new JButton("Go");
		moveButton.addActionListener(new ActionListener() 
		{	
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				queue.add(new MoveRobotEvent());
			}
		});
		eastPanel.add(moveButton);
		
		learn = new JRadioButton("nauczaj");
		learn.addActionListener(new ActionListener() 
		{	
			@Override
			public void actionPerformed(final ActionEvent arg0) 
			{
				queue.add(new LearnStatusEvent(learn.isSelected()));
			}
		});
		eastPanel.add(learn);
		
		cleanButton = new JButton("wyczysc pamiec");
		cleanButton.addActionListener(new ActionListener() 
		{	
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				queue.add(new CleanEvent());
			}
		});
		eastPanel.add(cleanButton);
	}

	private void setRobotPosition()
	{
		if(robotDto != null)
		{
			robott.setBounds((int) Math.round(robotDto.getX()*BACKGROUND_WIDTH -DISTANCE_TO_CENTER_FROM_LEFT*2), 
					(int) Math.round(robotDto.getY()*BACKGROUND_HEIGHT - robott.getPreferredSize().getHeight()/2), 
					(int)robott.getPreferredSize().getWidth(), (int)robott.getPreferredSize().getHeight());
			robott.setRotation(robotDto.getAngle());
			robott.setVisible(true);
		}
	}
	
	private void setDestinationPosition()
	{
		if(destinationDto != null)
		{
			destination.setBounds((int)(destinationDto.getX()*BACKGROUND_WIDTH - destination.getPreferredSize().getWidth()/2), 
					(int)(destinationDto.getY()*BACKGROUND_HEIGHT - destination.getPreferredSize().getHeight()/2), 
					(int)destination.getPreferredSize().getWidth(), (int)destination.getPreferredSize().getHeight());
			destination.setVisible(true);
		}
	}
	
	public void setBackground(final Image image)
	{
		background.setIcon(new ImageIcon(image.getScaledInstance(BACKGROUND_WIDTH, BACKGROUND_HEIGHT, Image.SCALE_DEFAULT)));
		background.setBounds(0,0, background.getIcon().getIconWidth(), background.getIcon().getIconHeight());
	}
	
	private Cursor getCursorFromImage(final ImageIcon image)
	{
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Cursor cursor = toolkit.createCustomCursor(image.getImage(), new Point(getX(), getY()), "myCursor");
		
		return cursor;
	}
	
	public void setRobotDto(final RobotDto dto)
	{
		this.robotDto = dto;
		setRobotPosition();
		repaint();
	}
	
	public void setDestinationDto(final DestinationDto dto)
	{
		this.destinationDto = dto;
		setDestinationPosition();
		repaint();
	}
}

package view;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Robot extends JLabel
{
	private static final long serialVersionUID = -4270590149676142004L;
	private final ImageIcon robotIcon;
	private BufferedImage robotImage;
	private AffineTransform transform;
	
	public Robot() 
	{
		super();
		robotIcon = new ImageIcon("img/robot8.png");
		try 
		{
			robotImage = ImageIO.read(new File("img/robot8.png"));
		} 
		catch (IOException e) 
		{
			System.out.println("nie udalo sie wczytac obrazka robota");
			e.printStackTrace();
		}
		setIcon(robotIcon);
		transform = new AffineTransform();
		this.setPreferredSize(new Dimension(robotIcon.getIconWidth()*2, robotIcon.getIconHeight()*2));
		transform.translate(robotIcon.getIconWidth()/2, robotIcon.getIconHeight()/2);
		
	}
	
	@Override
	protected void paintComponent(Graphics arg0) 
	{
		Graphics2D g2d = (Graphics2D)arg0;		
		g2d.drawImage(robotImage, transform, this);
		//super.paintComponent(arg0);
	}
	
	public void setRotation(final float angle)
	{
		transform = new AffineTransform();
		transform.translate(robotIcon.getIconWidth()/2, robotIcon.getIconHeight()/2);
		transform.translate(robotIcon.getIconWidth()/2, robotIcon.getIconHeight()/2);
		transform.rotate(angle);
		transform.translate(-robotIcon.getIconWidth()/2, -robotIcon.getIconHeight()/2);
	}
}

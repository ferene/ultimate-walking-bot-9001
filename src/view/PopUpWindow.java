package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.BlockingQueue;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;

import controller.event.ControllerEvent;
import controller.event.OpenFileEvent;

public class PopUpWindow extends JFrame
{
	private static final long serialVersionUID = 3079422737002911892L;
	@SuppressWarnings("unused")
	private final BlockingQueue<ControllerEvent> queue;
	private final JFileChooser fileChooser;
		
	public PopUpWindow(final BlockingQueue<ControllerEvent> queue) 
	{
		this.setTitle("Open File");
		this.queue = queue;
		fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileNameExtensionFilter("Bitmap", "bmp"));
		fileChooser.addActionListener(new ActionListener() 
		{	
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{				
				//wybrano jakis plik
				if(arg0.getActionCommand() == "ApproveSelection")
				{
					queue.add(new OpenFileEvent(fileChooser.getSelectedFile()));
				}
				// cancel 
				else if(arg0.getActionCommand() == "CancelSelection")
				{
					System.out.println("cancel");
				}
			
				//choosenFile = fileChooser.getSelectedFile();
				setVisible(false);
			}
		});
		this.add(fileChooser);
		pack();
		setVisible(false);
	}
	
	public void setVisible()
	{
		fileChooser.showOpenDialog(fileChooser);
	}
}

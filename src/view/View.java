package view;

import java.awt.Image;
import java.util.concurrent.BlockingQueue;

import javax.swing.SwingUtilities;

import controller.dto.DestinationDto;
import controller.dto.RobotDto;
import controller.event.ControllerEvent;


public class View 
{
	private final Screen screen;
	
	public View(final BlockingQueue<ControllerEvent> queue)
	{
		screen = new Screen(queue);
	}
	
	public void setBackground(final Image image)
	{
		SwingUtilities.invokeLater(new Runnable() 
		{	
			@Override
			public void run() 
			{
				screen.setBackground(image);
			}
		});
	}
	
	public void setRobotDto(final RobotDto dto)
	{
		SwingUtilities.invokeLater(new Runnable() 
		{	
			@Override
			public void run() 
			{
				screen.setRobotDto(dto);
			}
		});
	}
	
	public void setDestinationDao(final DestinationDto dto)
	{
		SwingUtilities.invokeLater(new Runnable() 
		{	
			@Override
			public void run() 
			{
				screen.setDestinationDto(dto);
			}
		});
	}

}

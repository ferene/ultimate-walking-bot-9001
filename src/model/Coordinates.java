package model;

public class Coordinates 
{
	private float x;
	private float y;
	
	public Coordinates(final float x, final float y) 
	{
		this.x = x;
		this.y = y;
	}

	public float getX() 
	{
		return x;
	}

	public float getY() 
	{
		return y;
	}
	
	public static double distance(final Coordinates position1, final Coordinates position2)
	{
		return Math.sqrt(Math.pow(position1.x - position2.x, 2) + Math.pow(position1.y - position2.y, 2));
	}
	
	public void move(double d, double e)
	{
		this.x += d;
		this.y += e;
	}
	
	public String toString()
	{
		return "(" + x + ";" + y + ")"; 
	}
}

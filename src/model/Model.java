package model;

import java.awt.image.BufferedImage;

import controller.dto.DestinationDto;
import controller.dto.RobotDto;

public class Model 
{
	private WalkingArea walkingArea;
	private Status learnStatus;
	private BufferedImage image;
	private Coordinates startCoordinates;
	private Coordinates stopCoordinates;
	private Robot robot;

	public Model()
	{
		learnStatus = Status.AUTO;
	}
	
	public void loadImage(final BufferedImage image) 
	{
		this.image = image;
	}
	
	public void placeRobot(final float x, final float y)
	{
		if (robot == null)
		{
			startCoordinates = new Coordinates(x * image.getWidth(), y * image.getHeight());
			if (stopCoordinates != null)
			{
				robot = new Robot(startCoordinates, stopCoordinates);
			}
		}
		else
		{
			robot.setCoordinates(x * image.getWidth(), y * image.getHeight());
		}
		
		if(image != null)
		{
			this.walkingArea = new WalkingArea(image, (int) (x * image.getWidth()), (int) (y * image.getHeight()));
		}
	}
	
	public void placeDestination(final float x, final float y)
	{
		if (robot == null)
		{
			stopCoordinates = new Coordinates(x * image.getWidth(), y * image.getHeight());
			if (startCoordinates != null)
			{
				robot = new Robot(startCoordinates, stopCoordinates);
			}
		}
		else
		{
			robot.setDestination(x * image.getWidth(), y * image.getHeight());
		}
	}
	
	public RobotDto getRobotDto()
	{ 
		if (robot != null)
			return robot.getRobotDto(image.getWidth(), image.getHeight());
		else if (startCoordinates != null)
			return new RobotDto(startCoordinates.getX() / image.getWidth(), startCoordinates.getY() / image.getHeight(), 0);
		else 
			return null;
	}

	public DestinationDto getDestinationDto()
	{ 
		if (robot != null)
			return robot.getDestinationDto(image.getWidth(), image.getHeight());
		else if (stopCoordinates != null)
			return new DestinationDto(stopCoordinates.getX() / image.getWidth(), stopCoordinates.getY() / image.getHeight());
		else 
			return null;
	}
	
	public void moveRobot(Direction direction)
	{

		if(robot != null && this.learnStatus == Status.LEARN)
			robot.move(direction, learnStatus, walkingArea);
	}
	
	public void changeLearnStatus()
	{
		if(this.learnStatus == Status.LEARN)
		{
			this.learnStatus = Status.AUTO;
		}
		else
			this.learnStatus = Status.LEARN;
	}
	
	public boolean robotAutoMove()
	{
		if(this.learnStatus == Status.AUTO)
			return robot.goToDestination(walkingArea);
		else
			return false;
	}
	
	public void clearRobotData()
	{
		if(robot!= null)
		{
			robot.clearData();	
		}
	}
}

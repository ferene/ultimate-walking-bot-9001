package model;

import java.util.Arrays;

import model.walk_model.AngleModel;
import model.walk_model.DecisionModel;
import model.walk_model.WalkModel;
import controller.dto.DestinationDto;
import controller.dto.RobotDto;

public class Robot 
{
	private final float MOVE_STEP = 2.5f;
	private Coordinates position;
	private Coordinates destination;
	private WalkModel walkModel;
	private float SENSOR_RANGE = 50;
	
	public Robot(Coordinates startCoordinates, Coordinates stopCoordinates)
	{
		position = startCoordinates;
		destination = stopCoordinates;
		walkModel = new AngleModel();
	}
	
	public void setCoordinates(final float x, final float y)
	{
		position = new Coordinates(x, y);
		walkModel.clearLastPosition();
	}
	
	public float getRobotAngle()
	{
		return (float) Math.atan2(destination.getY() - position.getY(), destination.getX() - position.getX());
	}
	
	private float getSensorReading(final double angle, WalkingArea walkingArea)
	{
		float x = position.getX();
		float y = position.getY();
		if(x < walkingArea.getX() && y < walkingArea.getY() && x >=0 && y>=0)
		{
			while(Math.round(x) >0 && Math.round(y) > 0 && Math.round(x) < walkingArea.getX() && Math.round(y) < walkingArea.getY() &&  walkingArea.isFieldWalkingPath(Math.round(x), Math.round(y)) )
			{
				y+= Math.sin(getRobotAngle() + angle);
				x+= Math.cos(getRobotAngle() + angle);
			}
			if(x >0 && y > 0 && x < walkingArea.getX() && y < walkingArea.getY())
			{
				float distance = (float) Math.sqrt( Math.pow((x - position.getX()), 2) + Math.pow(y - position.getY(), 2));
				if(distance > SENSOR_RANGE)
					return 1.0f;
				else 
					return distance/SENSOR_RANGE;
			}
			else
				return 1.0f;
		}
		else
			return 1.0f;
	}
	
	public void setDestination(final float f, final float g)
	{
		destination = new Coordinates(f, g);
	}

	public void move(Direction direction, Status learnStatus, WalkingArea walkingArea) 
	{
		float [] sample = new float[3];
		sample[0] = getSensorReading(-Math.PI/4, walkingArea);
		sample[1] = getSensorReading(0, walkingArea);
		sample[2] = getSensorReading(Math.PI/4, walkingArea);
		
		System.out.println("Read Sensors: " + Arrays.toString(sample));
		
		float angle = getRobotAngle();
		switch(direction)
		{
			case FORWARD: position.move(Math.cos(angle)*MOVE_STEP, Math.sin(angle)*MOVE_STEP);break;
			case BACK: position.move(-Math.cos(angle)*MOVE_STEP, -Math.sin(angle)*MOVE_STEP);break;
			case LEFT:position.move(Math.cos(angle - Math.PI/2)*MOVE_STEP, Math.sin(angle - Math.PI/2)*MOVE_STEP);break;
			case RIGHT:position.move(Math.cos(angle + Math.PI/2)*MOVE_STEP, Math.sin(angle + Math.PI/2)*MOVE_STEP);break;
		}
		if(learnStatus == Status.LEARN)
		{
			walkModel.addNewPosition(position, getRobotAngle(), sample);
		}
	}

	public RobotDto getRobotDto(int width, int height) 
	{
		return new RobotDto(position.getX() / width, position.getY() / height, getRobotAngle());
	}
	
	public DestinationDto getDestinationDto(int width, int height) 
	{
		return new DestinationDto(destination.getX() / width, destination.getY() / height);
	}

	public boolean goToDestination(WalkingArea walkingArea) 
	{
		double [] sample = new double[3];
		sample[0] = getSensorReading(-Math.PI/4, walkingArea);
		sample[1] = getSensorReading(0, walkingArea);
		sample[2] = getSensorReading(Math.PI/4, walkingArea);
		System.out.println("Read Sensors: " + Arrays.toString(sample));
		walkModel.walk(sample, position, getRobotAngle(), MOVE_STEP);
		return Coordinates.distance(position, destination) > 5;
	}
	
	public void clearData()
	{
		walkModel.clear();
	}
}

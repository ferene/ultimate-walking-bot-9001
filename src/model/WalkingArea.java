package model;

import java.awt.image.BufferedImage;


public class WalkingArea 
{
	private final int x;
	private final int y;
	private final boolean[][] walkingArea;
	
	public WalkingArea(final BufferedImage image, final int walkingPathX, final int walkingPathY)
	{
		int walkingPathNumber = image.getRGB(walkingPathX, walkingPathY);;
		
		this.x = image.getWidth();
		this.y = image.getHeight();
		walkingArea = new boolean[x][y];
		
		for(int j = 0; j< y; j++)
		{
			for(int i = 0; i< x; i++)
			{
				walkingArea[i][j] = image.getRGB(i, j) == walkingPathNumber;
			}
		}
		
//		for(int j = 0; j< y; j++)
//		{
//			for(int i = 0; i< x; i++)
//			{
//				if(i == 0 || i == x-1 || j == 0 || j == y-1)
//				{
//					walkingArea[i][j] = false;	
//				}
//			}
//		}
	}

	public int getX() 
	{
		return x;
	}

	public int getY() 
	{
		return y;
	}

	public boolean isFieldWalkingPath(final int x, final int y)
	{
		return walkingArea[x][y];
	}
	
	public String toString()
	{
		String result = "";
		
		for(int j = 0; j< y; j++)
		{
			for(int i = 0; i< x; i++)
			{
				result += (walkingArea[i][j]?1:0) + " ";
			}
			result += "\n";
		}
		return result;
	}

}

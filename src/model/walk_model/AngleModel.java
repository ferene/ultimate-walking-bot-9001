package model.walk_model;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import model.Coordinates;
import model.neural_network.NeuralNetwork;
import model.neural_network.Scale;

public class AngleModel implements WalkModel
{
	private final float MOVEMENT_STEP = 10.0f;
	private final Map<Float[], Double> samples;
	private Coordinates lastPosition;
	private Float[] lastSensorsReading; 
	private NeuralNetwork network;
	private int lastLearnSamplesNumber;
	private Scale inputScale;
	private Scale outputScale;
	
	public AngleModel()
	{
		samples = new HashMap<Float[], Double>();
		lastLearnSamplesNumber = 0;
	}
	
	private Coordinates rotate(Coordinates coordinates, float angle)
	{
		return new Coordinates((float)(coordinates.getX()*Math.cos(angle) - coordinates.getY()*Math.sin(angle)),
				(float)(coordinates.getX()*Math.sin(angle) + coordinates.getY()*Math.cos(angle)));
	}
	
	private double walkFunction(Coordinates position1, Coordinates position2, float angle)
	{
		Coordinates coord1 = rotate(position1, -angle);
		Coordinates coord2 = rotate(position2, -angle);
		return  (Math.atan2(coord2.getY()-coord1.getY(), coord2.getX()-coord1.getX()))/Math.PI;
	}
	
	private void reverceWalkFunction(double walkFunction, Coordinates position, float angle, float step)
	{
		Coordinates coords = rotate(new Coordinates((float)(step*Math.sin(walkFunction*Math.PI)),
				(float)(step * Math.cos(walkFunction * Math.PI))), -angle);
		position.move(coords.getY(), coords.getX());

	}
	
	public Float[] copyFloatArray(float [] data)
	{
		Float [] f = new Float[data.length];
		for (int i = 0; i < data.length; i++)
		{
			f[i] = new Float(data[i]);
		}
		return f;
	}
	
	public void addNewPosition(Coordinates position, float angle, float [] sensorsReading) 
	{
		assert sensorsReading.length == 3;
		if(this.lastPosition == null)
		{
			this.lastPosition =  new Coordinates(position.getX(), position.getY());
			this.lastSensorsReading = copyFloatArray(sensorsReading);
		}
		else
		{
			if(Coordinates.distance(lastPosition, position) > MOVEMENT_STEP)
			{
				System.out.println("Save Sample: " + Arrays.toString(lastSensorsReading) + " " + String.format("%.2f",walkFunction(lastPosition, position, angle)));
				this.samples.put(lastSensorsReading, walkFunction(lastPosition, position, angle));
				lastPosition = new Coordinates(position.getX(), position.getY());
				this.lastSensorsReading = copyFloatArray(sensorsReading);
			}
		}
	}

	private String printArray(double [] array)
	{
		DecimalFormat df = new DecimalFormat("0.00");
		String s = "[";
		for (int i = 0; i < array.length; i++)
		{
			s += df.format(array[i]) + ", ";
		}
		return s + "]";
	}
	
	public void walk(double [] sensorsReading, Coordinates actualPosition, float angle, float step) 
	{
		if(samples.size()>0)
		{
			if (network == null || lastLearnSamplesNumber < samples.size())
			{
				int [] hiddenLayers = new int[1];
				hiddenLayers[0] = 40;
				network = new NeuralNetwork(3, 1, hiddenLayers);
				double [][] input = new double [samples.size()][];
				double [][] output = new double [samples.size()][];
				int index = 0;
				for (Entry<Float [], Double> entry : samples.entrySet())
				{
					input[index] = new double[3];
					output[index] = new double[1];
					input[index][0] = entry.getKey()[0].doubleValue();
					input[index][1] = entry.getKey()[1].doubleValue();
					input[index][2] = entry.getKey()[2].doubleValue();
					output[index][0] = entry.getValue();
					index++;
				}
				inputScale = new Scale(input, 1.0);
				outputScale = new Scale(output, 1.73);
				
				network.learnSamples(input, output, 0.0000001);
				lastLearnSamplesNumber = input.length;
			}
			inputScale.scale(sensorsReading);
			double[] function = network.extrapolate(sensorsReading);
			outputScale.rescale(function);
			reverceWalkFunction(function[0], actualPosition, angle, step);
		}
	}
	
	public void clear()
	{
		samples.clear();
		network = null;
		lastPosition = null;
	}

	@Override
	public void clearLastPosition() 
	{
		lastPosition = null;
	}
}

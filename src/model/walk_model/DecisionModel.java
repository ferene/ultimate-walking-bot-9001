package model.walk_model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import model.Coordinates;
import model.neural_network.NeuralNetwork;
import model.neural_network.Scale;

public class DecisionModel implements WalkModel
{
	private final float MOVEMENT_STEP = 2.5f;
	private final Map<Float[], Decision> samples;
	private Coordinates lastPosition;
	private Float[] lastSensorsReading; 
	private NeuralNetwork network;
	private int lastLearnSamplesNumber;
	private Scale inputScale;
	
	private enum Decision
	{
		FORWARD(0),
		BACK(1),
		LEFT(2),
		RIGHT(3);
		
		private int index;
		private Decision(int value)
		{
			index = value;
		}
		public int getIndex()
		{
			return index;
		}
	}
	
	public DecisionModel()
	{
		samples =  new HashMap<Float[], Decision>();
	}
	
	private Decision getDecision(Coordinates position1, Coordinates position2, float angle)
	{
		float vectorX = position2.getX() - position1.getX();
		float vectorY = position1.getY() - position2.getY();
		double scalar = (vectorX*Math.cos(-angle)+vectorY*Math.sin(-angle))/Math.sqrt(vectorX*vectorX+vectorY*vectorY);
		
		if(scalar > 0.25)
			return Decision.FORWARD;
		
		if (scalar < -0.25)
			return Decision.BACK;
		
		if (vectorX*Math.sin(-angle)-vectorY*Math.cos(-angle) > 0)
			return Decision.RIGHT;
		else
			return Decision.LEFT;
	}
	

	public Float[] copyFloatArray(float [] data)
	{
		Float [] f = new Float[data.length];
		for (int i = 0; i < data.length; i++)
		{
			f[i] = new Float(data[i]);
		}
		return f;
	}
	
	@Override
	public void addNewPosition(Coordinates position, float angle, float [] sensorsReading) 
	{
		assert sensorsReading.length == 3;
		if(this.lastPosition == null)
		{
			this.lastPosition =  new Coordinates(position.getX(), position.getY());
			this.lastSensorsReading = copyFloatArray(sensorsReading);
		}
		else
		{
			if(Coordinates.distance(lastPosition, position) > MOVEMENT_STEP)
			{
				System.out.println("Save Sample: " + Arrays.toString(lastSensorsReading) + " " + getDecision(lastPosition, position, angle));
				this.samples.put(lastSensorsReading, getDecision(lastPosition, position, angle));
				lastPosition = new Coordinates(position.getX(), position.getY());
				this.lastSensorsReading = copyFloatArray(sensorsReading);
			}
		}
	}

	private void reverseDecision(Decision decision, Coordinates position, float angle, float step)
	{
		double vX = step*Math.cos(-angle);
		double vY = step*Math.sin(-angle);
		switch(decision)
		{
			case FORWARD:
				position.move(vX, -vY);break;
			case BACK:
				position.move(-vX, vY);break;
			case LEFT:;
				position.move(-vY, -vX);break;
			case RIGHT:
				position.move(vY, vX);break;
		}

	}
	
	@Override
	public void walk(double[] sensorsReading, Coordinates actualPosition, float angle, float step) 
	{
		if(samples.size()>0)
		{
			if (network == null || lastLearnSamplesNumber < samples.size())
			{
				int [] hiddenLayers = new int[2];
				hiddenLayers[0] = 20;
				hiddenLayers[1] = 20;
				network = new NeuralNetwork(3, 4, hiddenLayers);
				double [][] input = new double [samples.size()][];
				double [][] output = new double [samples.size()][];
				int index = 0;
				for (Entry<Float [], Decision> entry : samples.entrySet())
				{
					input[index] = new double[3];
					output[index] = new double[4];
					input[index][0] = entry.getKey()[0].doubleValue();
					input[index][1] = entry.getKey()[1].doubleValue();
					input[index][2] = entry.getKey()[2].doubleValue();
					output[index][0] = 0.0;
					output[index][1] = 0.0;
					output[index][2] = 0.0;
					output[index][3] = 0.0;
					output[index][entry.getValue().getIndex()] = 1.73;
					index++;
				}
				inputScale = new Scale(input, 1.0);
				
				network.learnSamples(input, output, 0.0001);
				lastLearnSamplesNumber = input.length;
			}
			inputScale.scale(sensorsReading);
			double[] function = network.extrapolate(sensorsReading);
			Decision temp = Decision.FORWARD;
			double max = function[0];
			if(function[Decision.BACK.getIndex()]>max)
			{
				max = function[Decision.BACK.getIndex()];
				temp = Decision.BACK;
			}
			if(function[Decision.LEFT.getIndex()]>max)
			{
				max = function[Decision.LEFT.getIndex()];
				temp = Decision.LEFT;
			}
			if(function[Decision.LEFT.getIndex()]>max)
			{
				max = function[Decision.LEFT.getIndex()];
				temp = Decision.LEFT;
			}
			reverseDecision(temp, actualPosition, angle, step);
		}
	}
	public void clear()
	{
		samples.clear();
		network = null;
		lastPosition = null;
	}
	
	@Override
	public void clearLastPosition() 
	{
		lastPosition = null;
	}
}

package model.walk_model;

import model.Coordinates;


public interface WalkModel 
{
	public void addNewPosition(Coordinates position, float angle, float [] sensorsReading);
	public void walk(double [] sensorsReading, Coordinates actualPosition, float angle, float step);
	public void clear();
	public void clearLastPosition();
}

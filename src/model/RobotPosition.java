package model;

public class RobotPosition 
{
	private float x;
	private float y;
	private float angle;
	
	public RobotPosition(final float x, final float y) 
	{
		this.x = x;
		this.y = y;
		this.angle = 0;
	}

	public float getX() 
	{
		return x;
	}

	public float getY() 
	{
		return y;
	}
		
	public void rotate(final float angle)
	{
		this.angle = (this.angle+angle)%360;
	}
	
	public void goForward(final float distance)
	{
		y-=distance * Math.sin(Math.toRadians(angle));
		x+=distance * Math.cos(Math.toRadians(angle));
	}
	
	public void goBack(final float distance)
	{
		goForward(-distance);
	}
	
	public void goLeft(final float distance)
	{
		y-=distance * Math.cos(Math.toRadians(angle));
		x-=distance * Math.sin(Math.toRadians(angle));
	}
	
	public void goRight(final float distance)
	{
		goLeft(-distance);
	}
	
	public void setAngle(final float angle)
	{
		this.angle = angle%360;	
	}
	
	public float getAngle()
	{
		return angle;
	}
	
	public static RobotPosition newInstance(final RobotPosition another)
	{
		RobotPosition robot = new RobotPosition(another.getX(), another.getY());
		robot.setAngle(another.getAngle());
		return robot;
		
	}
}

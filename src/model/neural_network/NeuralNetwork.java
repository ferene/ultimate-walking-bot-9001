package model.neural_network;

import java.util.Arrays;
import java.util.EmptyStackException;
import java.util.Random;


public class NeuralNetwork
{
	private final double [][] data;
	private final double [][][] wages;
	private final double e = 2.71828182845904523536028747135266249775724709369995;
	private final Random r = new Random();
	
	private double test(double a)
	{
		if (Double.isNaN(a))
		{
			System.out.println(Arrays.deepToString(data));
			System.out.println(Arrays.deepToString(wages));
			throw new EmptyStackException();
		}
		else 
			return a;
	}
	
	public NeuralNetwork(int numberOfInputs, int numberOfOutputs, int [] hiddenLayersSize)
	{
		int [] dataSize = new int [hiddenLayersSize.length+2];
		dataSize[0] = numberOfInputs;
		dataSize[hiddenLayersSize.length+1] = numberOfOutputs;
		for(int i = 0; i < hiddenLayersSize.length; i++)
		{
			dataSize[i+1] = hiddenLayersSize[i];
		}
		
		this.data = new double[dataSize.length][];
		for(int i = 0; i < dataSize.length; i++)
		{
			if(i != dataSize.length-1)
			{
				this.data[i] = new double[dataSize[i]+1]; 
				this.data[i][dataSize[i]] = 1.0;
			}
			else
				this.data[i] = new double[dataSize[i]];
			
			for (int j = 0; j < dataSize[i]; j++)
			{
				data[i][j] = 0.0f;
			}
		}

		
		this.wages = new double[dataSize.length-1][][];
		for(int i = 0; i < dataSize.length-1; i++)
		{
			this.wages[i] = new double[dataSize[i+1]][];
			for (int j = 0; j < this.wages[i].length; j++)
			{
				this.wages[i][j] = new double[dataSize[i]+1];
				for (int k = 0; k < dataSize[i]+1; k++)
				{
					if(i != dataSize.length-2)
						this.wages[i][j][k] = r.nextDouble()*2-1;
					else
						this.wages[i][j][k] = 0.0f;
				}
			}
		}
		
	}
	
	private double calculateSum(int layer, int neuron)
	{
		double sum = 0.0f;
		for (int k = 0; k < wages[layer][neuron].length; k++)
		{
			sum += wages[layer][neuron][k] * data[layer][k];
		}
		return sum;
	}
	
	public double[] extrapolate(double input [])
	{
		assert input.length == data[0].length;
		
		for (int i = 0; i < input.length; i++)
		{
			data[0][i] = input[i];
		}
		
		for(int i = 0; i < wages.length; i++)
		{
			for (int j = 0; j < wages[i].length; j++)
			{
				if(i+1 != data.length-1)
					data[i+1][j] = test(1/(1+Math.pow(e, -calculateSum(i, j))));
				else
					data[i+1][j] = test(calculateSum(i, j));
			}
		}
		
		double [] output = new double[data[data.length-1].length];
		for(int i = 0; i < output.length; i++)
		{
			output[i] = test(data[data.length-1][i]);
		}
		return output;
	}
	
	private double differential(double sum)
	{
		double EXP = Math.pow(e, -sum);
		return EXP/(1+EXP)/(1+EXP);
	}
	
	private void updateGradient(double [] input, double [] output)
	{
		assert input.length == data[0].length;
		assert output.length == data[data.length-1].length;
		
		double [] extrapolatedOutput = extrapolate(input);
		double [][] dataGradient = new double[data.length][];
		for (int i = 0; i < data.length; i++)
		{
			dataGradient[i] = new double[data[i].length];
			for (int j = 0; j < dataGradient[i].length; j++)
			{
				if (i != data.length - 1)
					dataGradient[i][j] = 0.0f;
				else
					dataGradient[i][j] = test(extrapolatedOutput[j] - output[j]);
			}
		}
		
		for (int i = data.length - 2; i > 0; i--)
		{
			for (int j = 0; j < data[i].length; j++)
			{
				for (int k = 0; k < wages[i].length; k++)
				{
					if(i != data.length - 2)
						dataGradient[i][j] +=test( dataGradient[i+1][k] * wages[i][k][j] * differential(calculateSum(i, k)));
					else
						dataGradient[i][j] += test(dataGradient[i+1][k] * wages[i][k][j]);
				}
			}
		}
		
		for (int i = 0; i < wages.length; i++)
		{
			for (int j = 0; j < wages[i].length; j++)
			{
				double sum = calculateSum(i, j);
				for (int k = 0; k < wages[i][j].length; k++)
				{
					double wageGradient  = 0.0f;
					if(i != wages.length - 1)
						wageGradient +=test( dataGradient[i+1][j] * differential(sum) * data[i][k]);
					else
						wageGradient +=test( dataGradient[i+1][j] * data[i][k]);
					this.wages[i][j][k] -= test(wageGradient);
				}
			}
		}
	}
	
	public void learnSamples(double [][] input, double [][] output, double maxError)
	{
		final float learnPercent = 0.8f;
		assert input.length == output.length;
		
		final int learnNumber = (int)(input.length * learnPercent);
		
		double [][] learnInput = new double [learnNumber][];
		double [][] learnOutput = new double[learnNumber][];
		
		for (int i = 0; i < learnNumber; i++)
		{
			int temp = r.nextInt(learnNumber);
			learnInput[i] = input[temp];
			learnOutput[i] = output[temp];
		}
		
		double lossFunction = 0.0f;
		for(int l = 0 ; l < 10000; l++)
		{
			for (int i = 0; i < learnNumber; i++)
			{
				updateGradient(learnInput[i], learnOutput[i]);
			}
			
			for (int m = 0; m < input.length; m++)
			{
				double error = 0.0f;
				double [] extrapolation =  extrapolate(input[m]);
				
				for (int i = 0; i<extrapolation.length; i++)
				{
					error += Math.pow(extrapolation[i]-output[m][i],2);
				}
				lossFunction += Math.sqrt(error);
			}
			lossFunction /= 2*input.length;
			if(lossFunction < maxError)
				break;
			lossFunction = 0.0f;
		}
		System.out.print(lossFunction);
	}
}

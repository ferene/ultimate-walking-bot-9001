package model.neural_network;

public class Scale 
{
	private double [] inputScale;
	private double [] inputShift;
	
	
	public Scale(double [][] input, double amplitude)
	{
		double [] inputSum = new double[input[0].length];
		double [] inputMin = new double[input[0].length];
		double [] inputMax = new double[input[0].length];
		inputScale = new double[input[0].length];
		inputShift = new double[input[0].length];
		
		for (int i = 0; i < input[0].length; i++)
		{
			inputSum[i] = 0;
			inputMin[i] = input[0][i];
			inputMax[i] = input[0][i];
		}
		for (int i = 0; i < input.length; i++)
		{
			for (int j = 0; j < input[i].length; j++)
			{
				inputSum[j] += input[i][j];
				if (inputMin[j] > input[i][j])
					inputMin[j] = input[i][j];
				if (inputMax[j] < input[i][j])
					inputMax[j] = input[i][j];
			}
		}
		for (int i = 0; i < input[0].length; i++)
		{
			inputShift[i] /= inputSum[i] / input.length;
			if ( inputShift[i] - inputMin[i] > inputMax[i] - inputShift[i])
			{
				inputScale[i] = (inputShift[i] - inputMin[i])/amplitude;
			}
			else
			{
				inputScale[i] = (inputMax[i] - inputShift[i])/amplitude;
			}
			if(inputScale[i] == 0.0)
			{
				inputScale[i] = 1/amplitude;
			}
		}
		
		
		for (int i = 0; i < input.length; i++)
		{
			for (int j = 0; j < input[i].length; j++)
			{
				input[i][j] = (input[i][j] - inputShift[j])/inputScale[j];
			}
		}
	}
	
	public void scale(double [] input)
	{
		for (int i = 0; i < input.length; i++)
		{
			input[i] = (input[i] - inputShift[i])/inputScale[i];
		}
	}
	
	public void rescale(double [] input)
	{
		for (int i = 0; i < input.length; i++)
		{
			input[i] = input[i]*inputScale[i]+inputShift[i];
		}
	}
}

package model;

public enum Direction 
{
	FORWARD,
	LEFT,
	RIGHT,
	BACK
}

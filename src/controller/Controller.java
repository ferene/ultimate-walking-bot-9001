package controller;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

import javax.imageio.ImageIO;

import model.Direction;
import model.Model;
import view.View;
import controller.event.CleanEvent;
import controller.event.ControllerEvent;
import controller.event.KeyPressedEvent;
import controller.event.LearnStatusEvent;
import controller.event.MoveRobotEvent;
import controller.event.OpenFileEvent;
import controller.event.SetDestinationPositionEvent;
import controller.event.SetRobotPositionEvent;


public class Controller 
{
	private final Model model;
	private final View view;
	private final BlockingQueue<ControllerEvent> queue;
	private Map<Class<? extends ControllerEvent>, ControllerReactionStrategy> strategyMap;
	
	public Controller(final Model model, final View view, final BlockingQueue<ControllerEvent> queue)
	{
		this.model = model;
		this.view = view;
		this.queue = queue;
		Map<Class<?extends ControllerEvent>, ControllerReactionStrategy> map =
				new HashMap<Class<?extends ControllerEvent>, ControllerReactionStrategy>();
		map.put(OpenFileEvent.class, new OpenFileStrategy());
		map.put(SetRobotPositionEvent.class, new SetRobotPositionStrategy());
		map.put(SetDestinationPositionEvent.class, new SetDestinationPositionStrategy());
		map.put(LearnStatusEvent.class, new LearnStatusStrategy());
		map.put(KeyPressedEvent.class, new KeyPressedStrategy());
		map.put(MoveRobotEvent.class, new MoveRobotStrategy());
		map.put(CleanEvent.class, new CleanStrategy());
		
		strategyMap = Collections.unmodifiableMap(map);
	}
	
	private abstract class ControllerReactionStrategy
	{
		public abstract void react(final ControllerEvent event);
	}
	
	private class OpenFileStrategy extends ControllerReactionStrategy
	{
		private OpenFileEvent fileEvent;
		@Override
		public void react(final ControllerEvent event) 
		{
			fileEvent = (OpenFileEvent) event;
			try 
			{
				Image imag = ImageIO.read(fileEvent.getFile());
				
				BufferedImage bufferedImage = new BufferedImage
				        (imag.getWidth(null),imag.getHeight(null),BufferedImage.TYPE_INT_ARGB);
				model.loadImage(bufferedImage);
				Graphics bg = bufferedImage.getGraphics();
 				bg.drawImage(imag, 0, 0, null);
 				bg.dispose();
				
				view.setBackground(imag);	
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	private class SetRobotPositionStrategy extends ControllerReactionStrategy
	{
		private SetRobotPositionEvent robotEvent;
		@Override
		public void react(final ControllerEvent event) 
		{
			robotEvent = (SetRobotPositionEvent) event;
			model.placeRobot(robotEvent.getX(), robotEvent.getY());
			transferRobotDto();
		}
	}
	
	public class SetDestinationPositionStrategy extends ControllerReactionStrategy
	{
		private SetDestinationPositionEvent positionEvent;
		@Override
		public void react(final ControllerEvent event) 
		{
			positionEvent = (SetDestinationPositionEvent) event;
			model.placeDestination(positionEvent.getX(), positionEvent.getY());
			transferDestinaionDto();
			transferRobotDto();	
		}
	}
	
	private class LearnStatusStrategy extends ControllerReactionStrategy
	{
		@Override
		public void react(ControllerEvent event) 
		{
			model.changeLearnStatus();
			transferRobotDto();
		}	
	}
	
	private class KeyPressedStrategy extends ControllerReactionStrategy
	{
		@Override
		public void react(final ControllerEvent event) 
		{
			switch(((KeyPressedEvent) event).getKeyCode())
			{
			case 'w': case KeyEvent.VK_UP:
				model.moveRobot(Direction.FORWARD);
				break;
			case 's': case KeyEvent.VK_DOWN:
				model.moveRobot(Direction.BACK);
				break;
				
			case 'a': case KeyEvent.VK_LEFT:
				model.moveRobot(Direction.LEFT);
				break;
				
			case 'd': case KeyEvent.VK_RIGHT:
				model.moveRobot(Direction.RIGHT);
				break;
				
			}
			transferRobotDto();	
		}
	}
	
	private class MoveRobotStrategy extends ControllerReactionStrategy
	{

		@Override
		public void react(ControllerEvent event) 
		{
			if(model.robotAutoMove())
			{
				transferRobotDto();
			}
		}
	}
	
	private class CleanStrategy extends ControllerReactionStrategy
	{
		@Override
		public void react(ControllerEvent event) 
		{
			model.clearRobotData();
		}
		
	}
	
	private void transferRobotDto()
	{
		view.setRobotDto(model.getRobotDto());
	}
	
	private void transferDestinaionDto()
	{
		view.setDestinationDao(model.getDestinationDto());
		
	}
	
	public void run()
	{
		while(true)
		{
			try 
			{
				ControllerEvent event = queue.take();
				strategyMap.get(event.getClass()).react(event);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
	}

}

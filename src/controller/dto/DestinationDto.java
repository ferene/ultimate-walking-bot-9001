package controller.dto;

import model.Coordinates;

public class DestinationDto 
{
	private final float x;
	private final float y;
	
	public DestinationDto(final float x, final float y) 
	{
		this.x = x;
		this.y = y;
	}

	public float getX() 
	{
		return x;
	}

	public float getY() 
	{
		return y;
	}

}

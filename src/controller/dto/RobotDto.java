package controller.dto;

import model.Coordinates;


public class RobotDto
{
	private final float x;
	private final float y;
	private final float angle;
	
	public RobotDto(final float x, final float y, final float angle) 
	{
		this.x = x;
		this.y = y;
		this.angle = angle;
	}

	public float getX() 
	{
		return this.x;
	}
	
	public float getY()
	{
		return this.y;
	}
	
	public float getAngle()
	{
		return this.angle;
	}
}

package controller.event;

public class KeyPressedEvent extends ControllerEvent
{
	private final char keyCode;
	
	public KeyPressedEvent(final char keyCode) 
	{
		this.keyCode = keyCode;
	}

	public char getKeyCode() 
	{
		return keyCode;
	}

}

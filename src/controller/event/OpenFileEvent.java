package controller.event;

import java.io.File;

/**
 * event otwarcia nowego pliku
 * 
 * @author ferene
 */
public class OpenFileEvent extends ControllerEvent
{
	/* otwirany plik */
	private final File file;
	
	/**
	 * konstruktor
	 * 
	 * @param file - otwierany plik
	 */
	public OpenFileEvent(final File file) 
	{
		this.file = file;
	}

	/**
	 * metoda zwracajaca plik
	 * 
	 * @return otwierany plik
	 */
	public File getFile() 
	{
		return file;
	}

}

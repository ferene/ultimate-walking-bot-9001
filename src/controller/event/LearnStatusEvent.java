package controller.event;

public class LearnStatusEvent extends ControllerEvent
{
	private final boolean learn;
	
	public LearnStatusEvent(final boolean learn) 
	{
		this.learn = learn;
	}

	public boolean isLearn() 
	{
		return learn;
	}

}

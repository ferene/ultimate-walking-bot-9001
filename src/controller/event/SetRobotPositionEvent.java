package controller.event;

/**
 * event postawienia robota
 * 
 * @author ferene
 */
public class SetRobotPositionEvent extends ControllerEvent
{
	private final float x;
	private final float y;
	
	public SetRobotPositionEvent(final float x, final float y) 
	{
		assert x <= 1.0f;
		assert y <= 1.0f;
		assert x >= 0.0f;
		assert y >= 0.0f;
		
		this.x = x;
		this.y = y;
	}

	public float getX() 
	{
		return x;
	}

	public float getY() 
	{
		return y;
	}
}

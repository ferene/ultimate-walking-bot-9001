import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import model.Model;
import view.View;
import controller.Controller;
import controller.event.ControllerEvent;


public class UltimateWalkingBot9001 
{
	public static void main(String[] args) 
	{

		final Model model = new Model();
		final BlockingQueue<ControllerEvent> queue = new LinkedBlockingQueue<ControllerEvent>();
		final View view = new View(queue);
		final Controller controller = new Controller(model, view, queue);
		
		controller.run();

		
	}
}
